//
//  Person.swift
//  Module1
//
//  Created by Waqas Haider on 3/18/24.
//

import Foundation

public class Person {
    let firstName: String
    let lastName: String
    
    public init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}
